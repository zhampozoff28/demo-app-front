import constants from "../constants/dictionaries.js";
/*
const revertObject = {
  methods: {
    async revertItemVersion(){

    }
    async checkRevertable(id, type){

    }
  }
}*/

const DictionaryMixin = {
    data(){
        return {
            dictionaries: constants
        };
    },
    methods: {
        async getDictionary(value){
            if (!value.remote){
                let res = [];
                value.values.forEach(v => {
                    res.push({
                        key: v,
                        name: this.$t('dictionaries.' + value.name + '.' + v)
                    })
                })
                return res;
            }
        }
    }
};
export {DictionaryMixin};
