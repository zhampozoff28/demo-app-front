const dictionaries = {
    ORGANIZATION_TYPES: {
        name: 'ORGANIZATION_TYPES',
        remote: false,
        values: ['RESTAURANT', 'CAFE', 'LOUNGE_BAR'],
    }
}
export default dictionaries;