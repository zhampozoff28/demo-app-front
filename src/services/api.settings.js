import axios from 'axios';

const currentVersion = 'v1';
const currentPort = 10000;
const currentHost = '127.0.0.1';

const core = axios.create({
    baseURL: 'http://' + currentHost + ':' + currentPort + '/api/' + currentVersion,
    proxy: {
        host: currentHost,
        port: currentPort,
    },
});


export {
    core
}