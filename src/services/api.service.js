import {core} from './api.settings'
import api from './api.constants'
export default {
    organization:{
        create(data){
            return core.post(api.organizations, data)
        }
    }
}