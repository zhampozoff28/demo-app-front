/*!

=========================================================
* Vue Argon Design System - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import Element from 'element-ui';
import './registerServiceWorker';
import i18n from './i18n/i18n';
import YmapPlugin from 'vue-yandex-maps'

Vue.config.productionTip = false;
const settings = {
    apiKey: '2b8f407d-313d-4d31-9008-075ee8aec817',
    lang: 'ru_RU',
    coordorder: 'latlong',
    version: '2.1'
}

Vue.use(YmapPlugin, settings)
Vue.use(Argon);
Vue.use(Element);
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})
new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount("#app");
