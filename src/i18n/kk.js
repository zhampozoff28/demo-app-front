export default {
    dictionaries: {
        ORGANIZATION_TYPES: {
            RESTAURANT: 'Ресторан',
            CAFE: 'Кафе',
            LOUNGE_BAR: 'Лаундж бар'
        }
    }
}