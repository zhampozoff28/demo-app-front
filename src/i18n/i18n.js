import Vue from 'vue';
import VueI18n from 'vue-i18n';
import ru from './ru';
import kk from './kk';
import ruLocale from 'element-ui/lib/locale/lang/ru-RU';
import kzLocale from 'element-ui/lib/locale/lang/kz';

Vue.use(VueI18n)

export default new VueI18n({
    locale: window.localStorage.getItem('lang') || 'kk',
    fallbackLocale: 'kk',
    messages: {
        ru: { ...ru, ...ruLocale },
        kk: { ...kk, ...kzLocale }
    }
})
